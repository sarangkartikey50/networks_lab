#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include <string.h>
int main() {
   int sockfd, portno, n;
   struct sockaddr_in serv_addr;
  // struct hostent *server;
   
   char buffer[256];
   
  /* if (argc < 3) {
      printf("2 arguements are required");
      exit(0);
   }*/
	
   portno = 7000;
   
   sockfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (sockfd < 0) {
     printf("error opening socket");
      exit(1);
   }
	
  
   
   memset((char *) &serv_addr,'\0',sizeof(serv_addr));
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr=inet_addr("127.0.0.1");
   serv_addr.sin_port = htons(portno);
   
   if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
      printf("error connecting");
      return 0;
   }
   
while(1)
	{
   printf("Please enter the message: ");
   memset(buffer,' ',256);
   fgets(buffer,255,stdin);
   
  
   n = write(sockfd, buffer, strlen(buffer));
   
   if (n < 0) {
      printf("ERROR writing to socket");
      return 0;
   }
   
   
   memset(buffer,' ',256);
   n = recv(sockfd, buffer, 250,0);
   
   if (n < 0) {
      printf("ERROR reading from socket");
      return 0;
   }
	
   printf("server:%s\n",buffer);
   }
   close(sockfd);
   return 0;
   
}
