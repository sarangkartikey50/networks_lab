#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include<math.h>
#include<errno.h>
#include<signal.h>

#define MAXLINE 100
#define SERV_PORT 5051

int max(int n1, int n2){
  if(n1 > n2) return n1;
  else return n2;
}

void str_echo(int sockfd){

  printf("client - %d\n", sockfd);

  char buffer[256];

  if (sockfd < 0) {
      perror("ERROR on accept");
      exit(1);
   }

   /* If connection is established then start communicating */
   bzero(buffer,256);
   int n = read( sockfd,buffer,255 );

   if (n < 0) {
      perror("ERROR reading from socket");
      exit(1);
   }

   printf("Here is the message: %s\n",buffer);
   strcpy(buffer, "message has been recieved");
   n = write(sockfd, buffer, strlen(buffer));

   if (n < 0) {
      perror("ERROR writing to socket");
      exit(1);
   }
}

void sig_chld(int pid){
  printf("child handler");
}

int main(int argc, char **argv){
 int     listenfd, connfd, udpfd, nready, maxfdp1;
 char    mesg[MAXLINE];
 pid_t   childpid;
 fd_set  rset;
 ssize_t n;
socklen_t len;
const int on = 1;
struct sockaddr_in cliaddr, servaddr;


/* create listening TCP socket */
listenfd = socket(AF_INET, SOCK_STREAM, 0);

bzero(&servaddr, sizeof(servaddr));
servaddr.sin_family = AF_INET;
servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
servaddr.sin_port = htons(SERV_PORT);

setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));
bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

listen(listenfd, 5);

/* create UDP socket */
udpfd = socket(AF_INET, SOCK_DGRAM, 0);
if(udpfd < 0){
  printf("error in creating udp socket");
  exit(0);
}

bzero(&servaddr, sizeof(servaddr));
servaddr.sin_family = AF_INET;
servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
servaddr.sin_port = htons(SERV_PORT);

bind(udpfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
signal(SIGCHLD, SIG_DFL);     /* must call waitpid() */

FD_ZERO(&rset);
maxfdp1 = max(listenfd, udpfd) + 1;
for ( ; ; ) {
 FD_SET(listenfd, &rset);
 FD_SET(udpfd, &rset);
 if ( (nready = select(maxfdp1, &rset, NULL, NULL, NULL)) < 0) {
   if(errno == EINTR)
      continue;
   else
      printf("select error");
  }

  if (FD_ISSET(listenfd, &rset)) {
      len = sizeof(cliaddr);
      connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &len);

      if ( (childpid = fork()) == 0) { /* child process */
          close(listenfd);     /* close listening socket */
          str_echo(connfd);    /* process the request */
          exit(0);
        }
      close(connfd);     /* parent closes connected socket */
  }

  if (FD_ISSET(udpfd, &rset)) {
    printf("udp client - %d\n", udpfd);
      len = sizeof(cliaddr);
      n = recvfrom(udpfd, mesg, MAXLINE, 0, (struct sockaddr *) &cliaddr, &len);
      printf("Recieved from server - %s", mesg);
      sendto(udpfd, mesg, n, 0, (struct sockaddr *) &cliaddr, len);
  }
 }
}
