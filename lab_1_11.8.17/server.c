#include<stdio.h>
#include<stdlib.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>

int main(){
  int sockfd, newsockfd, clilen, portno;
  char buffer[256];

  struct sockaddr_in serv_addr, client_addr;
  int n;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if(sockfd < 0){
    printf("Error in creating socket file descriptor!");
    exit(0);
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  portno = 6006;

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);

  if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
    printf("\nError in binding!");
    exit(0);
  }

  listen(sockfd, 5);
  clilen = sizeof(client_addr);

  newsockfd = accept(sockfd, (struct sockaddr *) &client_addr, &clilen);

  if(newsockfd < 0){
    printf("\nError in accepting request!");
    exit(0);
  }

  bzero(buffer, 255);
  n = read(newsockfd, buffer, 255);

  if(n < 0){
    printf("\nError in reading!");
    exit(0);
  }

  printf("\nMessage : %s", buffer);

  n = write(newsockfd, "I've got your message!", 18);

  if(n < 0){
    printf("\nError in writing");
    exit(0);
  }

  return 0;
}
