#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include<stdlib.h>

int main(){
  int clientSocket, portNum, nBytes;
  char buffer[256];
  struct sockaddr_in serverAddr;
  socklen_t addr_size;

  clientSocket = socket(PF_INET, SOCK_DGRAM, 0);

  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(5051);
  serverAddr.sin_addr.s_addr = (INADDR_ANY);
  memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

  addr_size = sizeof serverAddr;

  while(1){
    printf("send to server:\n");
    fgets(buffer,256,stdin);
    printf("You typed: %s",buffer);

    nBytes = strlen(buffer) + 1;

    sendto(clientSocket,buffer,nBytes,0,(struct sockaddr *)&serverAddr,addr_size);

    nBytes = recvfrom(clientSocket,buffer,256,0,NULL, NULL);
    if(nBytes < 0){
      printf("\nUnable to recieve!");
      exit(0);
    }

    printf("Received from server: %s\n",buffer);

  }

  return 0;
}
