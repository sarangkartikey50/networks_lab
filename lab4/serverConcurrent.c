#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include<stdlib.h>
#include<arpa/inet.h>
#include <string.h>

void msg(int sock);

int main( int argc, char *argv[] ) {
   int socketfd, newsockfd, portno, clilen;
   char buffer[256];
   struct sockaddr_in serv_addr, cli_addr;
   int n, pid;
   
   socketfd = socket(AF_INET, SOCK_STREAM, 0);
   
   if (socketfd < 0) {
      printf("ERROR opening socket");
      return 0;
   }
   
  
   memset((char *) &serv_addr,'\0',sizeof(serv_addr));
   portno = 7000;
   
   serv_addr.sin_family = AF_INET;
   serv_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
   serv_addr.sin_port = htons(portno);
   
   if (bind(socketfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
      printf("ERROR on binding");
      return 0;
   }
   
 
   listen(socketfd,5);
   clilen = sizeof(cli_addr);
   
   while (1) {
      newsockfd = accept(socketfd, (struct sockaddr *) &cli_addr, &clilen);
		
      if (newsockfd < 0) {
         printf("error on accept");
         return 0;
      }
      
      pid = fork();
		
      if (pid < 0) {
        printf("error occured while forking");
         return 0;
      }
      
      if (pid == 0) {
        
        while(1){
         close(socketfd);
         msg(newsockfd);
         // 
        
      }
           close(newsockfd);
      }
      else {
         close(newsockfd);
      }
		
   } 
close(socketfd);
return 0;
}
void msg(int sock) {
   int n;
   char buffer[256],msg[1000];
   bzero(buffer,256);
   n = read(sock,buffer,255);
   
   if (n < 0) {
      printf("error reading from socket");
      return;
   }
   
   printf("Here is the message: %s\n",buffer);
   printf("server:");
   scanf("%[^\n]s",msg);
//gets(msg);
   n=send(sock,msg,150,0);
   
   if (n < 0) {
      printf("error writing to socket");
     return ;
   }
	//close(sock);

}
