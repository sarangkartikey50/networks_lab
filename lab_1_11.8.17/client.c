#include<stdio.h>
#include<stdlib.h>
#include<netdb.h>
#include<netinet/in.h>
#include<string.h>

int main(int argc, char* argv[]){
  int sockfd, portno, n;
  struct hostent *server;
  struct sockaddr_in serv_addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);

  char buffer[256];

  portno = atoi(argv[1]);

  if(sockfd < 0){
    printf("\nError in opening socket!");
    exit(9);
  }


  //server = gethostbyname(hostname);

  /*if(server == NULL){
    printf("\nInvalid host");
    exit(0);
  }*/

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  //bcopy((char *) server->h_addr, (char *)serv_addr.sin_addr.s_addr, server->h_length);
  serv_addr.sin_port = htons(portno);

  if(connect(sockfd, (struct sock_addr*) &serv_addr, sizeof(serv_addr)) < 0){
    printf("\nError in connection!");
    exit(0);
  }

  printf("\nEnter message : ");
  bzero(buffer, 255);
  fflush(stdin);
  fgets(buffer, 255, stdin);

  n = write(sockfd, buffer, 255);

  if(n < 0){
    printf("\nError in sending!");
    exit(0);
  }

  n = read(sockfd, buffer, 255);

  if(n < 0){
    printf("\nError in reading!");
    exit(0);
  }

  printf("\nMessage from server : %s", buffer);

  return 0;
}
